from django.apps import AppConfig


class SplitterConfig(AppConfig):
    name = 'api.splitter'

    def ready(self):
        import api.splitter.signals
