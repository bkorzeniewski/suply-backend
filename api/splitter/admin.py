from django.contrib import admin
from .models import Splitter, Socket

admin.site.register(Splitter)
admin.site.register(Socket)
