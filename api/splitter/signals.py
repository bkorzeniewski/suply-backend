from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Splitter, Socket


@receiver(post_save, sender=Splitter)
def post_save_splitter(sender, instance,  **kwargs):
    if kwargs.get('created'):
        colors = ['0074D9', '01FF70', 'FFDC00', 'F012BE', ]
        for i in range(instance.number_sockets):
            socket = Socket(name='Socket '+str(i), color=colors[i], splitter=instance)
            socket.save()
