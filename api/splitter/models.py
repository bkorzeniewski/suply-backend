from django.db import models
from django.conf import settings
from api.location.models import Room


class Splitter(models.Model):
    name = models.CharField(max_length=200)
    number_sockets = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    room = models.ForeignKey(Room, blank=True, null=True, on_delete=models.CASCADE)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="splitters")
    mqtt_hash = models.OneToOneField('api.MqttHash', related_name='splitter', blank=True, null=True,
                                    on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Socket(models.Model):
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=6)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    splitter = models.ForeignKey(Splitter, on_delete=models.CASCADE, related_name="sockets")

    def __str__(self):
        return self.name


class SocketAlert(models.Model):
    range = models.FloatField()
    type = models.IntegerField()
    event_state = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    socket = models.ForeignKey(Socket, on_delete=models.CASCADE, related_name="alerts")

    def __str__(self):
        return str(self.range)+' '+str(self.type)


class SocketTimetable(models.Model):
    name = models.CharField(max_length=200)
    start = models.DateTimeField()
    end = models.DateTimeField()
    state = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    socket = models.ForeignKey(Socket, on_delete=models.CASCADE, related_name="timetable_items")

    def __str__(self):
        return self.name


class SocketData(models.Model):
    ampere = models.FloatField()
    volt = models.FloatField()
    state = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    socket = models.ForeignKey(Socket, on_delete=models.CASCADE, related_name="data_items")
