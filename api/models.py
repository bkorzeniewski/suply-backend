from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from api.splitter.models import Splitter, Socket
from .helpers import generate_key


class BaseApi(models.Model):
    splitters = models.ManyToManyField(Splitter, related_name='users_api')
    sockets = models.ManyToManyField(Socket, related_name='users_api')
    mqtt_hash = models.OneToOneField('api.MqttHash', related_name='user_api', blank=True, null=True,
                                     on_delete=models.CASCADE)

    def __str__(self):
        if hasattr(self, 'user'):
            return "(%s) %s" % (self.user.__class__.__name__, self.user.username)
        elif hasattr(self, 'app'):
            return "(%s) %s" % (self.app.__class__.__name__, self.app.name)
        return "None %d" % self.id


class User(AbstractUser):
    base_api = models.OneToOneField(BaseApi, related_name='user', blank=True, null=True, on_delete=models.CASCADE)


class App(BaseApi):
    name = models.CharField(max_length=200)
    contact_email = models.EmailField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='apps')

    def __str__(self):
        return self.name


class Token(models.Model):
    key = models.CharField(max_length=40, primary_key=True)
    base_api = models.OneToOneField(BaseApi, related_name='auth_token', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = generate_key(20)
        return super(Token, self).save(*args, **kwargs)

    def __str__(self):
        return self.key


class MqttHash(models.Model):
    password = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.password
