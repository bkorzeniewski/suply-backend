import hashlib, os, binascii
from base64 import b64encode
# From https://github.com/mitsuhiko/python-pbkdf2
from pbkdf2 import pbkdf2_bin


def make_hash(password):
    salt_length = 12
    key_length = 24
    hash_function = 'sha256'
    iteration = 901
    if isinstance(password, str):
        password = password.encode('utf-8')
    salt = b64encode(os.urandom(salt_length))
    return 'PBKDF2${}${}${}${}'.format(
        hash_function,
        iteration,
        str(salt, 'utf-8'),
        str(b64encode(pbkdf2_bin(password, salt, iteration, key_length, getattr(hashlib, hash_function))), 'utf-8'))


def generate_key(length):
    return binascii.hexlify(os.urandom(length)).decode()
