from django.contrib import admin

from .models import User, Token

admin.site.register(Token)
admin.site.register(User)
