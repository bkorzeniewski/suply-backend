from rest_framework import viewsets
from .serializers import UserSerializer, AppSerializer
from .models import User, App
from rest_framework import generics, permissions


class UserList(generics.ListCreateAPIView):
    model = User
    serializer_class = UserSerializer

    def get_queryset(self):
        queryset = User.objects.all()
        return queryset.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)