from rest_framework import serializers
from .models import House, Room, WiFi


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('id', 'name')


class HouseSerializer(serializers.ModelSerializer):
    rooms = RoomSerializer(many=True, read_only=True)
    
    class Meta:
        model = House
        fields = ('id', 'name', 'address', 'latitude', 'longitude', 'rooms')


class WiFiSerializer(serializers.ModelSerializer):
    class Meta:
        model = WiFi
        fields = ('ssid', 'password')
