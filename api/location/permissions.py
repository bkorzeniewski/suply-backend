from rest_framework import permissions


class IsOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        user = request.user
        if hasattr(obj, 'users') and user in obj.users.all():
            return True
        elif hasattr(obj, 'house') and user in obj.house.users.all():
            return True
        else:
            return False
