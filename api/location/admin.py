from django.contrib import admin

from .models import House, Room, WiFi, EnergySupplier

admin.site.register(House)
admin.site.register(Room)
admin.site.register(WiFi)
admin.site.register(EnergySupplier)
