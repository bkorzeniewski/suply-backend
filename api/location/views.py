from rest_framework import viewsets
from .serializers import HouseSerializer, RoomSerializer, WiFiSerializer
from .models import House, Room, WiFi
from rest_framework import generics
from .permissions import IsOwner
from django.shortcuts import get_object_or_404


class HouseList(generics.ListCreateAPIView):
    model = House
    serializer_class = HouseSerializer

    def get_queryset(self):
        queryset = House.objects.all()
        return queryset.filter(users=self.request.user)

    def perform_create(self, serializer):
        serializer.save(users=(self.request.user,))


class HouseDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsOwner,)
    serializer_class = HouseSerializer
    queryset = House.objects.all()
    model = House

    def get_object(self):
        obj = get_object_or_404(self.get_queryset(), pk=self.kwargs["pk"])
        self.check_object_permissions(self.request, obj)
        return obj


class RoomList(generics.ListCreateAPIView):
    model = Room
    serializer_class = RoomSerializer

    def get_queryset(self):
        print(self.kwargs["pkh"])
        house = get_object_or_404(House, pk=self.kwargs["pkh"])
        queryset = Room.objects.all()
        return queryset.filter(house=house)

    def perform_create(self, serializer):
        house = get_object_or_404(House, pk=self.kwargs["pkh"])
        serializer.save(house=house)


class RoomDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsOwner,)
    serializer_class = RoomSerializer
    model = Room

    def get_object(self):
        house = get_object_or_404(House, pk=self.kwargs["pkh"])
        obj = get_object_or_404(house.rooms.all(), pk=self.kwargs["pk"])
        self.check_object_permissions(self.request, obj)
        return obj



class WiFiViewSet(viewsets.ModelViewSet):
    queryset = WiFi.objects.all()
    serializer_class = WiFiSerializer

