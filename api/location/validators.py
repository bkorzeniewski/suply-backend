from django.core.exceptions import ValidationError


def validate_latitude(val):
    if not(-90 < val < 90):
        raise ValidationError("Błędna wartość")
    else:
        return val


def validate_longitude(val):
    if not(-180 < val < 180):
        raise ValidationError("Błędna wartość")
    else:
        return val
