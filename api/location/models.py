from django.db import models
from django.conf import settings
from .validators import validate_latitude, validate_longitude


class House(models.Model):
    name = models.CharField(default='House 1', max_length=200)
    address = models.TextField(blank=True, null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=8, blank=True, null=True, validators=[validate_latitude])
    longitude = models.DecimalField(max_digits=11, decimal_places=8, blank=True, null=True, validators=[validate_longitude])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='houses')

    def __str__(self):
        return self.name


class Room(models.Model):
    name = models.CharField(default='Room 1', max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    house = models.ForeignKey(House, on_delete=models.CASCADE, related_name="rooms")

    def __str__(self):
        return self.name


class WiFi(models.Model):
    ssid = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    house = models.ForeignKey(House, on_delete=models.CASCADE)

    def __str__(self):
        return self.ssid


class EnergySupplier(models.Model):
    name = models.CharField(max_length=200)
    averagePrice = models.FloatField()
    currency = models.CharField(max_length=3)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    house = models.ForeignKey(House, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
