from django.apps import AppConfig


class LocationConfig(AppConfig):
    name = 'api.location'
